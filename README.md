# example_gitlab_ci_pkgdown_and_devtools_check

This repo only contains a .gitlab-ci.yml file [here](example_gitlab_ci/.gitlab-ci.yml). Copy this file to the root of you R package gitlab repo to make gitlab ci run `devtools::check()` and deploy your pkgdown site. See [here](https://gitlab.com/-/profile/usage_quotas#pipelines-quota-tab) for your monthly quotas.
